﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/Waves"
{
	Properties
	{
		_Color("Color", Color) = (0, 0, 1, 1)
		_PeakColor("Peak Color", Color) = (1, 1, 1, 1)
		_NoiseMap("Noise", 2D) = "white" {}
		_NoiseAmp("Noise Amplification", float) = 1
		_WaveHeight("Wave height", float) = 0.5
		_WaveScale("Wave Scale", float) = 1
		_Speed("Speed", float) = 1.0
		_PeakPercent("Peak Percent", Range(0, 1)) = 0.8
		_RandomAdjust("Random Range" , Range(0, 1)) = 0.0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			float rand(float3 co)
			{
				return frac(sin(dot(co.xyz ,float3(12.9898,78.233,45.5432))) * 43758.5453);
			}

			float rand2(float3 co)
			{
				return frac(sin(dot(co.xyz ,float3(19.9128,75.2,34.5122))) * 12765.5213);
			}

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 color : COLOR;
			};

			fixed4 _Color;
			fixed4 _PeakColor;
			sampler2D _NoiseMap;

			float _WaveHeight;
			float _Speed;
			float _PeakPercent;
			float _RandomAdjust;
			float _WaveScale;
			float _NoiseAmp;
			
			v2f vert (appdata v)
			{
				v2f o;

				float height = _WaveHeight * (1 - _RandomAdjust);
				float randomHeight = _WaveHeight * _RandomAdjust;

				float phase0 = sin((_Time[1] * _Speed) + (v.vertex.x * _WaveScale * 2) + rand2(v.vertex.xxz));
				float phase1 = sin((_Time[1] * _Speed) + (v.vertex.x * _WaveScale * 3) + rand(v.vertex.zyx));

				float4 vertex = v.vertex;

				float noiseAdjust = tex2Dlod(_NoiseMap, float4(v.uv, 0, 0)).r * _NoiseAmp;
				float adjust = clamp(phase0 + phase1 - noiseAdjust, 0.0, 1.0);
				vertex.y += adjust * _WaveHeight;
				o.vertex = UnityObjectToClipPos(vertex);
				o.color = float4(0, 0, 0, adjust);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = _Color;
				if (i.color.w > _PeakPercent) {
					col = _PeakColor;
				}
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
