﻿using UnityEngine;

public class LoopAudioTrack : MonoBehaviour {

	public AudioClip Clip;

	private AudioSource _Source;

	private void Awake()
	{
		_Source = GetComponent<AudioSource>();
	}

	// Use this for initialization
	void Start () {
		_Source.clip = Clip;
	}
	
	// Update is called once per frame
	void Update () {
		if (!_Source.isPlaying)
		{
			_Source.Play();
		}
	}
}
