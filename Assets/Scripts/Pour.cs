﻿using System.Collections;
using UnityEngine;

public class Pour : MonoBehaviour {

	public ParticleSystem Particles;
	public Transform Box;
	public GameObject Credit;
	public Animator Animator;

	public float PoorTime;

	private bool _RunningCoroutine;
	private bool _Down;

	private float _RemainingPoor;

	private void Awake()
	{
		Credit.SetActive(false);
	}

	void Start () {
		_RemainingPoor = PoorTime;
	}
	
	void Update () {
		if (_RemainingPoor <= 0f)
		{
			return;
		}

		Animator.enabled = false;
		if (Input.anyKey && !_RunningCoroutine && !_Down)
		{
			StartCoroutine(PitchToAngle(80f, 2f));
			_Down = true;
		}
		else if (!Input.anyKey && !_RunningCoroutine && _Down)
		{
			StartCoroutine(PitchToAngle(30f, 2f));
			_Down = false;
		}

		if (Box.localEulerAngles.x > 50f && !Particles.isPlaying)
		{
			Particles.Play();
		}
		else if (Box.localEulerAngles.x < 50f && Particles.isPlaying)
		{
			Particles.Stop();
		}

		if (Particles.isPlaying && !_RunningCoroutine)
		{
			_RemainingPoor -= Time.deltaTime;
			if (_RemainingPoor < 0f)
			{
				StartCoroutine(EndGame());
			}
		}
	}

	IEnumerator PitchToAngle(float angle, float duration)
	{
		_RunningCoroutine = true;
		var time = 0f;
		var startEuler = Box.localEulerAngles;
		var targetEuler = Box.localEulerAngles;
		targetEuler.x = angle;
		while (time < duration)
		{
			time += Time.deltaTime;
			var easeInOut = Easing.EaseInOut(time / duration, 2);
			Box.localEulerAngles = Vector3.Lerp(startEuler, targetEuler, easeInOut);
			yield return 0;
		}
		_RunningCoroutine = false;
	}

	IEnumerator LowerBox()
	{
		var start = Box.localPosition;
		var target = start;
		target.y -= 5f;
		target.z -= 4f;
		var time = 0f;
		var duration = 2f;
		while (time < duration)
		{
			time += Time.deltaTime;
			var easeInOut = Easing.EaseInOut(time / duration, 2);
			Box.localPosition = Vector3.Lerp(start, target, easeInOut);
			yield return 0;
		}
	}

	IEnumerator EndGame()
	{
		Particles.Stop();
		while (_RunningCoroutine)
		{
			yield return 0;
		}
		StartCoroutine(LowerBox());
		if (Box.localEulerAngles.x > 40f)
		{
			StartCoroutine(PitchToAngle(10, 2f));
			while (_RunningCoroutine)
			{
				yield return 0;
			}
		}
		yield return new WaitForSeconds(3f);
		Credit.SetActive(true);
		yield return new WaitForSeconds(3f);
		Application.Quit();
	}
}
