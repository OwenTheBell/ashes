using UnityEngine;

/*
* This is a library of Easing functions to be used with the default unity Lerp
* functionality. Provided with a percent value [0-1], these functions return
* a float which can then be used for the t value in the unity Lerp functions.
* All of the functions assume a beginning of 0 and an end of 1.
*/

public static class Easing {

	const float BOUNCE_FORCE = 1.70158f;

	public static float EaseIn(float t, int pow) {
		return Mathf.Pow(t, pow);
	}

	public static float EaseOut(float t, int pow) {
		if (pow % 2 == 0) {
			return -1 * (Mathf.Pow(t - 1, pow) - 1);
		}
		return Mathf.Pow(t - 1, pow) + 1;
	}

	public static float EaseInOut(float t, int pow) {
		t /= 0.5f;
		if (t < 1) {
			return 0.5f * Mathf.Pow(t, pow);
		}
		if (pow % 2 == 0) {
			return -0.5f * (Mathf.Pow(t-2, pow) - 2);
		}
		return 0.5f * (Mathf.Pow(t-2, pow) + 2);
	}

	public static float EaseInBack(float t, int pow) {
		float bounce = BOUNCE_FORCE * pow;
		return t*t*((bounce + 1f)*t + bounce) + 1;
	}

}
